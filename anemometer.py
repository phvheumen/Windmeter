#!/usr/bin/env python3
#

# Core modules
from collections import deque
from struct import unpack
import itertools
import socket
import time
# External modules
import smbus

DEVICE_ADDRESS = 0x68
I2C_BUS = 1

bus = smbus.SMBus(I2C_BUS)

# Continious, Channel 1, 18-bit, PGA 2
config = 0b10011101

UDP_IP = "127.0.0.1"
UDP_PORT = 5757

print("UDP target IP:", UDP_IP)
print("UDP target port:", UDP_PORT)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP


moving_average = deque([0 for _ in range(0,600)],600)

def slice_deque(q, start, stop):
    return list(itertools.islice(q, start, stop))

while True:
    epoch = time.time()

    data = bus.read_i2c_block_data(0x68, config, 3)
    data = bytearray([0] + data)
    code = unpack('>l', data)
    code = code[0]

    FS_CODE = 131071 # Full Scale code [-]
    FEG = (33/180)  # Front-end Gain [-]
    VREF = 2.048  # Reference Voltage [V]
    PGA = 2  # Programmable Gain [-]
    V = (code/FS_CODE)*(VREF/PGA)*(1/FEG)  # Measured Voltage [V]
    I = 1000*V/249  # Converted to Current [mA]
    BIAS = 0.121 # Zero speed bias [m/s]
    speed = (I-4)*(50/16) - BIAS # Converted to Speed [m/s]
    speed = max(0.0, speed)
    moving_average.appendleft(speed)

    # Calculate the moving average in different time windows
    speed_1min = sum(slice_deque(moving_average, 0, 60))/60
    speed_5min = sum(slice_deque(moving_average, 0, 300))/300
    speed_10min = sum(slice_deque(moving_average, 0, 600))/600

    # Build InfluxDB query and push over UDP
    # It is the task of Telegraf to catch and process this data
    query = "windspeed value={:.4f},avg_1min={:.4f},avg_5min={:.4f},avg_10min={:.4f}".format(speed, speed_1min, speed_5min, speed_10min)
    sock.sendto(bytes(query, "utf-8"), (UDP_IP, UDP_PORT))

    # Sleep Zzzz...
    time.sleep((epoch + 1.0) - time.time())
