#!/usr/bin/env python3
#

# Core modules
import io
import socket
import time
# External modules
import pynmea2
import serial

ser = serial.Serial()
ser.port = '/dev/ttyAMA0'
ser.baudrate = 4800
ser.timeout = 1
ser.open()

sio = io.TextIOWrapper(io.BufferedRWPair(ser, ser), encoding='UTF-8')

UDP_IP = "127.0.0.1"
UDP_PORT = 5757

print("UDP target IP:", UDP_IP)
print("UDP target port:", UDP_PORT)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP

while True:
    try:
        line = sio.readline()
        msg = pynmea2.parse(line)
        if(isinstance(msg, pynmea2.GGA)):
            decimal_lattitude = float(msg.lat[0:2]) + float(msg.lat[2:-1])/60
            decimal_longitude = float(msg.lon[0:3]) + float(msg.lon[3:-1])/60
            query = "gps lat={:.7f},lon={:.7f},altitude={},num_sats={}".format(decimal_lattitude, decimal_longitude, msg.altitude, int(msg.num_sats))
            sock.sendto(bytes(query, "utf-8"), (UDP_IP, UDP_PORT))
            continue
        if(isinstance(msg, pynmea2.GSA)):
            query = "gps pdop={},hdop={},vdop={}".format(msg.pdop, msg.hdop, msg.vdop)
            sock.sendto(bytes(query, "utf-8"), (UDP_IP, UDP_PORT))
            continue
        if(isinstance(msg, pynmea2.GSV)):
            continue
        if(isinstance(msg, pynmea2.RMC)):
            continue
        
        print(msg.sentence_type)

    except serial.SerialException as e:
        print("Device error: {}".format(e))
        break
    except pynmea2.ParseError as e:
        print("Parse error: {}".format(e))
        continue
    except ValueError as e:
        print("Decode error: {}".format(e))
    except KeyboardInterrupt as e:
        print("Received ctrl+C, exit...")
        exit(0)


