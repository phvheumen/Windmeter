#!/usr/bin/env bash
#

# Install prerequisites
sudo /usr/bin/python -m pip install -r requirements.txt

sudo install ./anemometer.py /usr/bin/
sudo install ./anemometer.service /etc/systemd/system/
sudo install ./gps.py /usr/bin/
sudo install ./gps.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable anemometer.service
sudo systemctl enable gps.service
sudo systemctl restart anemometer.service
sudo systemctl restart gps.service
